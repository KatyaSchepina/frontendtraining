$(document).ready(function() {
    var corpsesFilter = $('#Сorps');
    var profilesFilter = $('#Profile');
    var auditoryFilter = $('#Auditory');

    var dictionary = {};

    fetch('https://lyceumexams.herokuapp.com/api/dictionary')
        .then(res => res.json())
        .then(function(res) {
            dictionary = res;
            for (let [key, value] of Object.entries(res.corpses)) {
                corpsesFilter.append(`<option value="${key}">${value} </option>`)
            }
        });

    corpsesFilter.change(function() {
        profilesFilter.find('option').not(':first').remove();
        auditoryFilter.find('option').not(':first').remove();

        let corpsesFilterValue = corpsesFilter.val();

        if (!corpsesFilterValue) {
            return;
        }

        fetch('https://lyceumexams.herokuapp.com/api/corpses/' + corpsesFilter.val())
            .then(res => res.json())
            .then(function(res) {
                res.places.forEach((place) => {
                    profilesFilter.append(`<option value="${place._id}">${place.code} </option>`)
                });
            });

        renderPeople();
    })

    profilesFilter.change(function() {
        auditoryFilter.find('option').not(':first').remove();

        fetch('https://lyceumexams.herokuapp.com/api/corpses/' + corpsesFilter.val())
            .then(res => res.json())
            .then(function(res) {
                let places = profilesFilter.val() ? [res.places.find(p => p._id == profilesFilter.val())] : res.places;
                places.forEach(place => place.audience.forEach((audience) => auditoryFilter.append(`<option value="${audience._id}">${audience.name} </option>`)));
            });

        renderPeople();
    })

    auditoryFilter.change(function() {
        renderPeople();
    })

    function renderPeople() {
        let corp = corpsesFilter.val();
        let profile = profilesFilter.val();
        let auditory = auditoryFilter.val();

        $('#students > tbody').find("tr").remove();

        fetch("https://lyceumexams.herokuapp.com/api/pupils?corps=" + corp + "&place=" + profile)
            .then(res => res.json())
            .then(res => {
                console.log(res);
                let pupils = auditory ? res.filter(person => person.audience == auditory) : res;
                pupils.forEach((person, index) => {
                    let tr = '<tr>';
                    tr += '<td>' + index + '</td>';
                    tr += '<td>' + person.firstName + " " + person.lastName + " " + person.parentName + '</td>';
                    tr += '<td>' + dictionary.audiences[person.audience] + '</td>';
                    tr += '<td>' + dictionary.places[person.place].code + '</td>';
                    tr += '</td>';
                    $('#students > tbody').append(tr);
                });
            })
    }
});